local mod_name = minetest.get_current_modname()

local function log(level, message)
    minetest.log(level, ('[%s] %s'):format(mod_name, message))
end

local function safe(func)
    -- wrap a function w/ logic to avoid crashing the game
    local f = function(...)
        local status, out = pcall(func, ...)
        if status then
            return out
        else
            log('warning', 'Error (func):  ' .. out)
            return nil
        end
    end
    return f
end

log('action', 'CSM loading...')

minetest.register_chatcommand('intro', {
    params = '<playernamename>',
    description = 'say FL intro to player',
    func = safe(function(param)
        local username = param
    local message = "Welcome to Linux-Forks! Please, read the rules at https://li-fo.de/rules ! If you would like a free apartment with free food (take as you need) then type /phw !"
    minetest.send_chat_message(username .. ": " .. message)
    end),
})


